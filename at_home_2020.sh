#!/bin/bash

# valid on linux mint 20 64bits

sudo apt install kupfer baobab keepassx terminator atop catfish liblo-tools guvcview bless pdfshuffler kpcli tor openssh-server filezilla chromium-browser lynx clementine vlc comix audacity inkscape slic3r meshlab font-manager blender obs-studio

# dev suite for polymorph (godot and PE)
sudo apt install build-essential make cmake automake scons libtool libboost-chrono-dev libboost-thread-dev libboost-date-time-dev mesa-common-dev libxt-dev libxaw7-dev libxrandr-dev libxxf86vm-dev libglu-dev libglu1-mesa-dev alsa-tools alsa-utils alsa-source libasound2-dev libcppunit-dev doxygen pkg-config libgtk2.0-dev nvidia-cg-toolkit libx11-dev libxcursor-dev libxinerama-dev libgl1-mesa-dev libgl1-mesa-dev libgl1-mesa-dev libgl1-mesa-dev libgl1-mesa-dev libxi-dev libudev-dev libpulse-dev yasm git meld cmake-qt-gui