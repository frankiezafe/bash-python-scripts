# notes about sftp #

## setup ##

install openssh on all machines

 sudo apt install openssh-server

## how-to ##

connection to remote machine

 $ sftp [username]@[local_ip]
 
enter the password of your user

 sftp> cd [path]
 
### to download a complete folder ###

**first**: navigate to local folder! Launch sftp **IN** the local directory

 $ cd [local directory]
 $ sftp [username]@[local_ip]
 
 sftp> get -r [distant directory]
 
### to upload a complete folder ###

**first**: navigate to distant folder! Launch command put **IN** the distant directory

 $ sftp [username]@[local_ip]
 sftp> cd [distant directory]
 sftp> put -r [local directory]
 
### to exit ###

 sftp> exit