#!/bin/bash

# this script purpose is to keep a process running even if it crashes from time to time
# it loops every seconds (see line 86), and shutdowns the computer at specified hour

# for the sound, if it doesn't work... http://askubuntu.com/questions/409860/hdmi-audio-on-default-alsa-device-in-xubuntu

# ********* configuration of the script *********
# root password, for shutdown
ROOT_PASSWORD="sudo password"
# full path to the exec FOLDER
EXECPATH="/folder/containing/the/exec"
# name of the exec in the process list
EXECNAME="name of the process (use $ top to verify)"
# exec command, with arguments if required
EXEC="./exec_file"

# shutdown hour and minute
SHUTDOWN_HOUR=21
SHUTDOWN_MIN=15
# date where shutdown must be skipped, vernissage for instance, or late opening
# note that comupter must be stopped manually these dates
# encode the dates in a list of YYYYMMDD, space separated
SKIP_DATE="20171026 20171027"

# ********* global vars *********
SHUTDOWN_SKIP=0
INIT=1
TODAY=$(date +"%Y%m%d")

# ********* skip dates test *********
for i in ${SKIP_DATE}
do
	if [[ "$TODAY" == "$i" ]];
	then
		#echo ${TODAY}'  is a skip date!';
		SHUTDOWN_SKIP=1 
	fi
done

if [[ "$SHUTDOWN_SKIP" == 1 ]];
then
	echo 'skipping shutdown today!';
else
	echo 'this is a normal date, automatic shutdown enabled!';
fi

# ********* infinite loop *********
while :

do

	if [ $INIT -eq 1 ]; then
		# removing any screen or power saver that might be on
		xset -dpms s 0 0 s noblank s noexpose s off
        # hide mouse
        xinit -- -nocursor
		INIT=0
		echo "INIT"
	fi

	pid=$(pidof ${EXECNAME})
	#echo $pid

	hour=$( date +"%H" )
	min=$( date +"%M" )

	if [[ "$SHUTDOWN_SKIP" == 0 ]];
	then
		if [[ $(echo "( $hour == $SHUTDOWN_HOUR)" | bc) -ne 0 ]]; then
			if [[ $(echo "( $min == $SHUTDOWN_MIN)" | bc) -ne 0 ]]; then
				echo "Goodbye everybody..."
				# one minute delay to kill process manually, machine recovery or late intervention
				sleep 60
				echo "${ROOT_PASSWORD}" | sudo -S poweroff
			fi
		fi
	fi

	if [ $pid -gt 0 ]; then
		echo "${EXECNAME} is running, everything's fine at ${hour}:${min}"
	else
		cd ${EXECPATH}
		${EXEC} > /dev/null &
		echo "${EXECNAME} launched!!!"
	fi

	sleep 1

done
