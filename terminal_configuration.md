== Line return ==

How to have a line-return at the end of each line in the terminal.

$ echo $PS1
display current configuration for each line in terminal

copy the result, we'll have to paste it in 2 files

On linux mint 18, this value is:

 \[\e]0;\u@\h \w\a\]${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\] \[\033[01;34m\]\w \$\[\033[00m\]

add a '\n' at the end of the line

edit .profile

add:

 PS1=[new value with \n]

no spacing!

edit .bashrc

add:

 PS1=[new value with \n]

login & logout, done!
