#!/bin/bash
# https://stackoverflow.com/questions/1885525/how-do-i-prompt-a-user-for-confirmation-in-bash-script

INSTALL_TOOLS=0

echo "Do you want to install tools: tortoisehg, cmake-qt-gui, sublime-text & blender2ogre?"
read -p "'y' to confirm: " -n 1 -r
echo
if [[ $REPLY =~ ^[Yy]$ ]]
then
	INSTALL_TOOLS=1
	echo "Ok, let's do it!"
    # do dangerous stuff
else
	echo "Ok, skipping"
fi

if [[ $INSTALL_TOOLS = 1 ]]
then
	echo "Installing tools"
fi
