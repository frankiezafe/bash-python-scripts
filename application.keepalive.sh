#!/bin/bash

# script to schedule an exec running hours
# adapt the start and stop hours in the variable section
# WARNING: NO LEADING 0 IN HOURS AND MINUTES

# disabling power management and screensaver
xset -dpms
xset s blank
xset s off

#################
# CONFIGURATION #
#################

# start hour
ka_startH=8
ka_startM=45

# stop hour
ka_stopH=19
ka_stopM=0

# sleep duration
ka_sleep=0.1

#############
# MAIN LOOP #
#############

ka_start=$(( $ka_startH * 600 ))
ka_start=$(( $ka_start + $ka_startM ))

ka_stop=$(( $ka_stopH * 600 ))
ka_stop=$(( $ka_stop + $ka_stopM ))

password=0
appname="[the name of exec, visible via 'top' if not sure]"
apppath="[full path to exec folder]"

echo "$appname : start hour: $ka_start ( $ka_startH:$ka_startM ) - $ka_stop ( $ka_stopH:$ka_stopM )"

while :
do

	ka_hs=$(date +%H)
	ka_ms=$(date +%M)
	ka_time=$(( $ka_hs * 600 ))
	ka_time=$(( $ka_time + $ka_ms ))

	v=$(pidof $appname)

	if [[ "$ka_time" -ge "$ka_start" ]] && [[ "$ka_time" -lt "$ka_stop" ]]; then

		# DAY!!!
		# verifying that everything is working
		if [[ "$v" -eq "" ]]; then

			cd "$apppath"
			"$(./$appname &)"
			echo "$appname started at $ka_hs:$ka_ms"

		fi

	else

		# NIGHT!!!
		# stopping genealogy
		if [[ "$v" -ne "" ]]; then
			killall "$appname"
		fi
		
		# shutting down
		if [[ "$ka_time" -gt "$ka_stop" ]]; then
			echo $password | sudo -S shutdown -h now
		fi

	fi

	sleep $ka_sleep

done
