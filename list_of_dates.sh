#!/bin/bash

TODAY=$(date +"%Y%m%d")
SKIP_DATE="20171023"
SHUTDOWN_SKIP=0

for i in ${SKIP_DATE}
do
    if [[ "$TODAY" == "$i" ]];
    then
        echo ${TODAY}'  is a skip date!';
        SHUTDOWN_SKIP=1 
    fi
done

if [[ "$SHUTDOWN_SKIP" == 1 ]];
then
    echo 'skipping shutdown today!';
else
    echo 'this is a normal date, automatic shutdown enabled!';
fi
