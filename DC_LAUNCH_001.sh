#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

function polymorph_comment(){
echo "+-------------------------------------------------+"
echo "|  _________ ____  .-. _________/ ____ .-. ____   |"
echo "|  __|__  (_)_/(_)(   )____<    \|    (   )  (_)  |"
echo "|                  ˇ                   ˇ          |"
echo "+-------------------------------------------------+"
echo "|                                                 |"
echo "| $comment"
echo "|                                                 |"
echo "+-------------------------------------------------+"
}

function polymorph_subcomment(){
echo "+-------------------------------------------------+"
echo "|                                                 |"
echo "| $comment"
echo "|                                                 |"
echo "+-------------------------------------------------+"
}

comment="launching empty.0.1"
polymorph_comment

cd /home/frankiezafe/projects/disrupted-cities/polymorph/DR_mesh_generation/build/dist/bin/ && ./DC

comment="see you soon (^3^)"
polymorph_subcomment