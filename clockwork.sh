#!/bin/bash

# CONFIGURATION
password="alarm"
# duration in seconds
ka_sleep=1
# duration in minutes
ka_max_runningtime=120

# WORKING VARS
td=$(date +%Y%m%d)
ths=$(date +%H)
tms=$(date +%M)
ka_previoustime=$(( $td * 100000 ))
ka_previoustime=$(( $ka_previoustime + $ths * 600 ))
ka_previoustime=$(( $ka_previoustime + $tms ))
ka_runningtime=0

while :
do
	ka_d=$(date +%Y%m%d)
	ka_hs=$(date +%H)
	ka_ms=$(date +%M)
	ka_time=$(( $ka_d * 100000 ))
	ka_time=$(( $ka_time + $ka_hs * 600 ))
	ka_time=$(( $ka_time + $ka_ms ))
	
	ka_deltams=$(( $ka_time - $ka_previoustime ))
	ka_runningtime=$(( $ka_runningtime + $ka_deltams ))
	#echo "h: $ka_hs ,m: $ka_ms => $ka_time <> $ka_previoustime, running time: $ka_runningtime"
	
	if [ $ka_runningtime -gt $ka_max_runningtime ]; then
		echo "max running time reached, let's reboot!"
		echo $password | sudo -S reboot
	fi
	ka_previoustime=$ka_time
	sleep $ka_sleep
	
done
