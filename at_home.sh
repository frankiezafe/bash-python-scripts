#!/bin/bash

# system
# get latest versions of nvidia's drivers - once done, check with sudo apt install nvidia-[tab]
sudo add-apt-repository ppa:graphics-drivers/ppa
sudo apt update

# utils
sudo apt install kupfer baobab keepassx terminator atop shutter catfish liblo-tools guvcview bless pdfshuffler

#web
sudo apt install tor openssh-server riot-web filezilla chromium-browser lynx

# media
sudo apt install clementine vlc comix  

# graphics & 3d
sudo apt install audacity digikam inkscape makehuman makehuman-blendertools makehuman-clothes makehuman-hair makehuman-skins makehuman-data slic3r meshlab font-manager

# dev suite for godot
sudo apt-get install build-essential scons pkg-config libx11-dev libxcursor-dev libxinerama-dev libgl1-mesa-dev libglu-dev libasound2-dev libpulse-dev libudev-dev libxi-dev libxrandr-dev yasm clang-7

# dev suite for polymorph (godot and PE)
sudo apt install build-essential make cmake automake scons libtool libboost-chrono-dev libboost-thread-dev libboost-date-time-dev mesa-common-dev libxt-dev libxaw7-dev libxrandr-dev libxxf86vm-dev libglu-dev libglu1-mesa-dev alsa-tools alsa-utils alsa-source libasound2-dev libcppunit-dev doxygen pkg-config libgtk2.0-dev nvidia-cg-toolkit libx11-dev libxcursor-dev libxinerama-dev libgl1-mesa-dev libgl1-mesa-dev libgl1-mesa-dev libgl1-mesa-dev libgl1-mesa-dev libxi-dev

## dev suite: versioning
sudo apt install mercurial git git-cola tortoisehg

## dev suite: IDE & editors
sudo apt install kdevelop atom meld cmake-qt-gui

# notepadqq
sudo add-apt-repository ppa:notepadqq-team/notepadqq && sudo apt-get update && sudo apt-get install notepadqq

# obs
sudo add-apt-repository ppa:obsproject/obs-studio && sudo apt-get update && sudo apt-get install obs-studio

# bouml
# wget -q http://www.bouml.fr/bouml_key.asc -O- | sudo apt-key add -
# echo "deb http://www.bouml.fr/apt/zesty zesty free" | sudo tee -a /etc/apt/sources.list
# sudo apt update && sudo apt install bouml
