#!/bin/bash

gpufeedback=""
gputemp=0
gputempconfort=66
gputempmax=70

cd /home/burningman/Documents/of_v0.7.4_linux64_release/apps/myApps/fragments4344/bin/
./fragments4344 &

while true
do

	gpufeedback=$(aticonfig --od-gettemperature)
	gputemp=${gpufeedback:85:2}
	echo "GPU temp: $gputemp"
	if(($gputemp>=$gputempmax))
	then
		echo "GPU temperature above $gputempmax!!! : $gputemp"
		killall fragments4344 &

		#waiting for temp to go down
		
		while(($gputemp>$gputempconfort))
		do
			sleep 5
			gpufeedback=$(aticonfig --od-gettemperature)
			gputemp=${gpufeedback:85:2}
			clear
			echo "new check of GPU temperature: $gputemp"
			sensors
		done		
		
		./fragments4344 &

	fi
	sleep 1

done