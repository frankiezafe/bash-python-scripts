#!/bin/bash

width=60;
height=60;
x_offset=10;
y_offset=5;
filelist=`ls | grep '.jpg'`
prefix="cropped"

destination='cropped'

# removing the destination folder
rm -r -f ${destination}
# creating a new destination folder
mkdir -p ${destination}

counter=1
for image_file in $filelist
do
	target_file=${prefix}_${image_file}
	echo ${counter} :: ${target_file}
	counter=`expr $counter + 1`
	convert $image_file -crop ${width}x${height}+${x_offset}+${y_offset} ${destination}/${target_file}

done