#!/bin/bash

# utilisation de la raspberry avec un écran VGA
# liste de choses à faire dans le terminal
# redimesionner la vidéo avec ffmpeg
# ffmpeg -i fichier.mp4 -vf scale=768x768 -an -q:v 1 sortie.mp4
# sudo cp /boot/config.txt /boot/config.txt.BU
# sudo leafpad /boot/config.txt
# >> hdmi_group=2
# >> hdmi_mode=16
# >> config_hdmi_boost=4
# dans la barre de menu, click droit sur la barre
# avncé >> réduire >> taille 0 pixels
# sudo chmod +x autoplay.sh << rendre executable
# leafpad .bashrc
# ajouter ./autoplay.sh dans le fond

# desactivation du screensaver
xset -dpms
xset s blank
xset s off

# lancement de la video
omxplayer --loop --no-osd Videos/horloge.mp4
