#!/bin/bash

# use a fixed width
width=1200

# use percentage
#percent=50

quality=85

filelist=`ls | grep '.JPG$'`
prefix="resized"

destination='resized'

# removing the destination folder
rm -r -f ${destination}
# creating a new destination folder
mkdir -p ${destination}

counter=1
for image_file in $filelist
do
	target_file=${prefix}_${image_file}
	echo ${counter} :: ${target_file}
	counter=`expr $counter + 1`
	convert $image_file -resize ${width} -quality ${quality}% ${destination}/${target_file}
	#convert $image_file -resize ${percent}% -quality ${quality}% ${destination}/${target_file}

done
