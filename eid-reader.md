# using eID on linux

I'm using a Dectel CI692 reader, not detected correctly. I had to to do the following to make it run.

## arch linux

install required libraries

```
$ sudo pacman -S opensc pcsc-tools ccid

// enforce 'cac' drivers

$ sudo nano /etc/opensc.conf

// add 'card_drivers = cac' and 'force_card_driver = cac' to the configuration

app default {
        # debug = 3;
        # debug_file = opensc-debug.txt;
        framework pkcs15 {
                # use_file_caching = true;
        }
}

card_drivers = cac
force_card_driver = cac

// save and quit

// start service
$ systemctl start pcscd.socket
$ systemctl enable pcscd.socket
```

plug your card reader

verify your car reader is listed:

```
$ opensc-tool --list-readers
```

should returns something like this:

```
# Detected readers (pcsc)
Nr.  Card  Features  Name
0    Yes             Generic Smart Card Reader Interface [Smart Card Reader Interface] (20070818000000000) 00 00
```

(not required) logout and login, card reader should stay connected


locate 'opensc-pkcs11.so':

```
$ whereis opensc-pkcs11.so
opensc-pkcs11: /usr/lib/opensc-pkcs11.so
```

## linux mint 20.1

reference: https://wiki.debian.org/BeID

```
$ sudo apt install opensc pcsc-tools
```

locate 'opensc-pkcs11.so':

```
$ dpkg --listfiles opensc-pkcs11 | grep /opensc-pkcs11.so
/usr/lib/x86_64-linux-gnu/opensc-pkcs11.so
/usr/lib/x86_64-linux-gnu/pkcs11/opensc-pkcs11.so
```

## firefox

go to **settings** -> **settings & privacy** [link](about:preferences#privacy) -> **security devices**

click on **Load**

- module name: **Opensc PKCS#11 Module**
- path: `/usr/lib/opensc-pkcs11.so` for **arch**, `/usr/lib/x86_64-linux-gnu/opensc-pkcs11.so` for linux mint
