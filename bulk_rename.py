#!/usr/bin/python
import os,re

ext = ".png"
prefix = "newname_"
startat = 0 # in case bulk is done in several passes (warning!!! -> zerocount maybe incorrect!)

files = os.listdir('.')
files.sort()
fcount = len( files ) * 1.
zerocount = 1
while fcount > 1:
	zerocount += 1
	fcount /= 10
fcount = startat
for f in files:
	# skip all files which are not .mp4
	if not f.endswith(ext):
		continue
	fname = ""+str(fcount)
	while( len( fname ) < zerocount ):
		fname = "0"+fname
	fname = prefix+fname+ext
	os.rename(f, fname)
	fcount += 1

	
