import os,re,subprocess
from time import gmtime, strftime

extension = ".avi"

output_path = "vlist.txt"

output = open(output_path,"w") 
files = os.listdir('.')
for f in files:
	if not f.endswith(extension):
		continue
	output.write( "file '" + f + "'\n")
output.close()

subproccall = "ffmpeg -f concat -i " + output_path + " -c copy " + strftime("%Y%m%d%H%M%S", gmtime()) + "_concat" + extension
subprocess.call(subproccall.split( " " ))
os.remove( output_path )
