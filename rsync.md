
## torrent backup

```
rsync --verbose --update --recursive --progress /home/frankiezafe/torrent/ /run/media/frankiezafe/Elements/torrent 
```

## multithread rsync

rsync -avr src/ dest/

cd src; ls -1 | xargs -n1 -P10 -I% rsync -ar % dest/

## prevent disk to sleep during rsync!

sudo apt install sdparm
sudo sdparm --clear=STANDBY /dev/sdb -S

rsync -avzx --progress --delete --stat src dest

## --stat is not recognised anymore...
rsync -avzx --recursive --progress src dest

rsync -avzx --recursive --progress /home/frankiezafe/ home.frankiezafe/
rsync -avzx --recursive --progress /opt/ opt
rsync -avzx --recursive --ignore-existing --progress
