# mediaserver in meyboom

## files

to access shared folders, open your file browser, go to *Network* and locate a machine named **MEDIASERVER**

connect to it and use this credentials:

- user: dj
- psswrd: dj

music folder is named **mediaserver_music**, feel free to pick what you want

if you want to share you library, make sure the structure is: 

*Artist name* / *Album name* / *track number* - *track name*

another folder named **public** is available to share files locally, just make sure you remove heavy folder once the work is done, we only have 500Gb of space on this drive

## player and remote control (for studio 5)

Clementine player is launched at startup on this computer. You can take control on the playback and playlists by installing a remote control app on your smartphone.

Go to fdroid and install [Clementine remote control](https://f-droid.org/en/packages/de.qspool.clementineremote/)

Make sure you are connected on the **MEYBOOM_WIFI** or **hacktiris_...**.

Open Clementine Remote, enter **mediaserver** in the server field and hit **Connect**.

You can play tracks or download them on your smartphone.

## mounting on linux

you just have to mount the music folder to add all the music to your local player

to do so, install cifs utils:

**debian**:

```
sudo apt-get install cifs-utils
```

**arch**:

```
sudo pacman -S cifs-utils
```

create a folder somewhere in your disk

```
mkdir MSZIK
```

and mount the drive in your folder

```
sudo mount //mediaserver.local/mediaserver_music -o username=dj,password=dj,rw,nounix,iocharset=utf8,file_mode=0644,dir_mode=0755 ~/MSZIK
```

to unmount the drive, just do:

```
sudo umount ~/MSZIK
```

### automation:

to automate this action, create a mszik_mount.sh file in your user root with this code:

```
#!/bin/bash
sudo mount //mediaserver.local/mediaserver_music -o username=dj,password=dj,rw,nounix,iocharset=utf8,file_mode=0644,dir_mode=0755 ~/MSZIK
```

and add the execution flag with 

```
chmod +x mszik_mount.sh
```

once done, you should be able to do:

```
./mszik_mount.sh
```

## ssh

to reconfigure samba shares, connect to server via ssh:

```
ssh dj@mediaserver.local
```

password: **dj**

and edit config with:

```
sudo nano /etc/samba/smb.conf
```

once done, restart samba with:

```
sudo service smbd restart
```
