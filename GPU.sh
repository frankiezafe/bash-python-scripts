#!/bin/bash

gpufeedback=""
gputemp=0
gputempmax=60 

while true
do
	gpufeedback=$(aticonfig --od-gettemperature)
	gputemp=${gpufeedback:85:2}
	if(($gputemp>=$gputempmax))
	then
		echo "GPU temperature above $gputempmax!!! : $gputemp"
	fi
	#echo ${gputemp/2}
	sleep 1
done