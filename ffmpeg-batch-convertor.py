#!/usr/bin/python
import os,re, subprocess

#to_mjpeg()
#to_lowres_mp4()
#to_wav()

def to_mjpeg():
    ext = ".MTS"
    prefix = "mjpegs/"
    startat = 0
    files = os.listdir('.')
    files.sort()
    fcount = len( files ) * 1.
    zerocount = 1
    while fcount > 1:
	    zerocount += 1
	    fcount /= 10
    fcount = startat
    for f in files:
	    if not f.endswith(ext):
		    continue
	    fname = f + "_" + str(fcount)
	    while( len( fname ) < zerocount ):
		    fname = "0"+fname
	    fname = prefix+fname+".mkv"
	    print "******************************************\n" + f + " STARTS"
	    subprocess.call(["ffmpeg", "-i", f, "-q:v", "1", "-an", "-flags", "-ildct-ilme", "-deinterlace", "-vcodec", "mjpeg", fname])
	    print fname + " DONE"
	    fcount += 1

def to_lowres_mp4():
    ext = ".MTS"
    prefix = "mp4/"
    startat = 0
    files = os.listdir('.')
    files.sort()
    fcount = len( files ) * 1.
    zerocount = 1
    while fcount > 1:
	    zerocount += 1
	    fcount /= 10
    fcount = startat
    for f in files:
	    if not f.endswith(ext):
		    continue
	    fname = f + "_" + str(fcount)
	    while( len( fname ) < zerocount ):
		    fname = "0"+fname
	    fname = prefix+fname+".mp4"
	    print "******************************************\n" + f + " STARTS"
	    subprocess.call(["ffmpeg", "-i", f, "-q:v", "1", "-an", "-flags", "-ildct-ilme", "-deinterlace", "-vf", "scale=iw*0.25:ih*0.25", "-vcodec", "libx264", fname])
	    print fname + " DONE"
	    fcount += 1

def to_wav():
    ext = ".MTS"
    prefix = "../wav/"
    startat = 0
    files = os.listdir('.')
    files.sort()
    fcount = len( files ) * 1.
    zerocount = 1
    while fcount > 1:
	    zerocount += 1
	    fcount /= 10
    fcount = startat
    for f in files:
	    if not f.endswith(ext):
		    continue
	    fname = f + "_" + str(fcount)
	    while( len( fname ) < zerocount ):
		    fname = "0"+fname
	    fname = prefix+fname+".wav"
	    print "******************************************\n" + f + " STARTS"
	    subprocess.call(["ffmpeg", "-i", f, "-q:v", "1", "-vn", fname])
	    print fname + " DONE"
	    fcount += 1

